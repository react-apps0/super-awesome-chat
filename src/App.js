import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ChatWindow from './chatComponents/ChatWindow'

/*
The instructions are included in the `instructions.md` file.
*/

const users = [{ username: 'Chioma' }, { username: 'Steve' }];

const messages = [
  { username: 'Chioma', text: 'Hi Love!' },
  { username: 'Chioma', text: 'How are you?' },
  { username: 'Steve',  text: 'Hi Ormahlicham! Good, you?' },
];

class App extends Component {
  state = {
	msgArr: messages
  }
    
  addMessage = message => {
    this.setState((currState) => ({
    	msgArr: [...currState.msgArr, message]
    }))
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactND - Coding Practice</h1>
        </header>
    	<div className="container">
		  {users.map(user => (
           	<ChatWindow 
           		user={user} 
				msgArr={this.state.msgArr} 
				addMessage={this.addMessage}
			/>
           ))}
      	</div>
	  </div>
    );
  }
}

export default App;
