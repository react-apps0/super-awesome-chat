import React from 'react';
import '../App.css';
import PropTypes from 'prop-types'

function Message(props) {
  const {user, message, index} = props
  return (
    	<li
        	key={index}
            className={ message.username === user.username ? 'message sender' : 'message recipient'}
    	>
    		<p>{`${message.username}: ${message.text}`}</p>
        </li>
   )
}

Message.propTypes = {
  user: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired
}


export default Message