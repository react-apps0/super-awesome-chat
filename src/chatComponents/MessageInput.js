import React, { Component } from 'react';
import '../App.css';
import PropTypes from 'prop-types'

class MessageInput extends Component {
  static propTypes = {
    username: PropTypes.string.isRequired,
    onAddMessage: PropTypes.func.isRequired
  }

  state = {
    text: ''
  }
  
  updateText = event => {
    this.setState({
    	text: event.target.value
    })
  }
  
  clearText = () => {
    this.setState({
    	text: ''
    })
  }
  
  handleSubmit = event => {
    event.preventDefault() 
    const message = { username: this.props.username, text: this.state.text }
    //hoist all the way up to App.js where it's added to messages
    this.props.onAddMessage(message)
    this.clearText()
  }
  /*
  If the user did not type anything, he/she should not be allowed to submit.
  */
  isDisabled = () => {
   return this.state.text === ''
  }
  
  render() {
    return (
      <div>
        <form className="input-group" onSubmit={this.handleSubmit}>
          <input 
            type="text" 
            className="form-control" 
            placeholder="Enter your message..." 
            value={this.state.text}
            onChange={this.updateText}
          />
          <div className="input-group-append">
              <button className="btn submit-button" disabled={this.isDisabled()}>
                  SEND
              </button>
          </div>
        </form>
      </div>
   ) 
  }
}

export default MessageInput