import React from 'react';
import '../App.css';
import PropTypes from 'prop-types'
import ListMessage from './ListMessage'
import MessageInput from './MessageInput'

function ChatWindow(props) {
  
  const {user, msgArr, addMessage} = props

  return (
      <div className="chat-window">
        <h2>Super Awesome Chat</h2>
        <div className="name sender">{user.username}</div>
        <ListMessage msgArr={msgArr} user={user}/>
        <MessageInput username={user.username} onAddMessage={addMessage}/>
      </div>
  )
}

ChatWindow.propTypes = {
  user: PropTypes.object.isRequired,
  msgArr: PropTypes.array.isRequired,
  addMessage: PropTypes.func.isRequired
}

export default ChatWindow