import React from 'react';
import '../App.css';
import PropTypes from 'prop-types'
import Message from './Message'

function ListMessage(props) {
  const {msgArr, user} = props
 
  return (
    <ul className="message-list">
        {msgArr.map((msg, index) => (
    		<Message user={user} message={msg} index={index} />
        ))}
     </ul>
  )
}

ListMessage.propTypes = {
  msgArr: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired
}

export default ListMessage